class CreateEstimates < ActiveRecord::Migration
  def change
    create_table :estimates do |t|
      t.string :name
      t.string :contact
      t.text :content
      t.references :request, index: true

      t.timestamps null: false
    end
    add_foreign_key :estimates, :requests
  end
end
