class EstimatesController < ApplicationController
  def new
    @estimate=Estimate.new
    @request=Request.find(params[:r_id])
  end
  
  def create
     @request=Request.find(params[:estimate][:r_id])
     @estimate=@request.estimates.build(params.require(:estimate).permit(:name,:contact,:content))
     if @estimate.save
        redirect_to root_path
      else
        render new
    end
  end


  def edit
    
  end
  
  def show
    @estimate=Estimate.find(params[:id])
    @request=@estimate.request
  end

  
  def index
    @requests=Request.all
  end
end
